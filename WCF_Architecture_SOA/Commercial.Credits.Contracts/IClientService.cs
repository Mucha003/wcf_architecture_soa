﻿namespace Commercial.Credits.Contracts
{
    using Domain;
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;

    [ServiceContract]
    public interface IClientService
    {
        [OperationContract]
        // Service Rest Attr
        [WebGet (
            RequestFormat  = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate    = "/GetClient/{pDocumentID}",
            BodyStyle      = WebMessageBodyStyle.Bare)]
        [FaultContract(typeof(Erreurs))]
        Client GetClient(string pDocumentID);

        [OperationContract]
        // Service Rest Attr
        [WebGet(
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetAllClient",
            BodyStyle = WebMessageBodyStyle.Bare)]
        IEnumerable<Client> GetAll();
    }
}
