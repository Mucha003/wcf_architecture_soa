﻿namespace Commercial.Credits.Contracts
{
    using Domain;
    using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;

    [ServiceContract]
    public interface ICreditService
    {
        [OperationContract]
        // Service Rest Attr
        [WebGet(
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "/GetAll",
            BodyStyle = WebMessageBodyStyle.Bare)]
        IEnumerable<Credit> GetAll();

        [OperationContract]
        [WebInvoke(
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            Method = "POST",
            UriTemplate = "/RegistrerCredit",
            BodyStyle = WebMessageBodyStyle.Bare)]
        Credit RegistrerCredit(Credit credit);

        [OperationContract]
        [WebInvoke(
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            Method = "PUT",
            UriTemplate = "/UpdateCredit",
            BodyStyle = WebMessageBodyStyle.Bare)]
        Credit UpdateCredit(Credit credit);

        [OperationContract]
        [WebInvoke(
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            Method = "DELETE",
            UriTemplate = "/DeleteCredit/{IdCredit}",
            BodyStyle = WebMessageBodyStyle.Bare)]
        bool DeleteCredit(string IdCredit);
    }
}
