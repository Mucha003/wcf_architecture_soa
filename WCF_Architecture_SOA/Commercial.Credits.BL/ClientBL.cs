﻿namespace Commercial.Credits.BL
{
    using Domain;
    using RepositoryContrat;
    using SQLRepository;
    using System;
    using System.Collections.Generic;

    public class ClientBL : IDisposable
    {
        private readonly IClientRepository _repos ;
        public ClientBL()
        {
            this._repos = new ClientRepository();
        }


        public IEnumerable<Client> GetAll()
        {
            return _repos.GetAll();
        }


        public Client GetClient(string pDocumentID)
        {
            return _repos.GetClient(pDocumentID);
        }


        public void Dispose()
        {
            // Libere Nos ressources.
            GC.SuppressFinalize(this);
        }
    }
}
