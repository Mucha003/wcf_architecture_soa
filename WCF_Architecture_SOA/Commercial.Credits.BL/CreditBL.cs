﻿namespace Commercial.Credits.BL
{
    using Domain;
    using RepositoryContrat;
    using SQLRepository;
    using System;
    using System.Collections.Generic;

    public class CreditBL : IDisposable
    {
        private readonly ICreditRepository _repos;
        public CreditBL()
        {
            this._repos = new CreditRepository();
        }

        public IEnumerable<Credit> GetAll()
        {
            return _repos.GetAll();
        }

        public Credit RegistrerCredit(Credit credit)
        {
            return _repos.RegistrerCredit(credit);
        }

        public Credit UpdateCredit(Credit credit)
        {
            return _repos.UpdateCredit(credit);
        }

        public bool DeleteCredit(string IdCredit)
        {
            return _repos.DeleteCredit(IdCredit);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
