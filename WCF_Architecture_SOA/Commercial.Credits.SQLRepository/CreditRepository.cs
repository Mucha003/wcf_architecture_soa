﻿namespace Commercial.Credits.SQLRepository
{
    using Dapper;
    using Domain;
    using RepositoryContrat;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    public class CreditRepository : ICreditRepository
    {
        public IEnumerable<Credit> GetAll()
        {
            using (var _db = new SqlConnection(ConnectionRepository.SqlConnection()))
            {
                _db.Open();
                var _creditList = _db.Query<Credit>("sp_Credit_getAllCredits", commandType: CommandType.StoredProcedure);
                return _creditList;
            }
        }


        public Credit RegistrerCredit(Credit credit)
        {
            using (var _db = new SqlConnection(ConnectionRepository.SqlConnection()))
            {
                _db.Open();

                var CreditParams = new DynamicParameters();
                CreditParams.Add("IdCredit", credit.Id, DbType.Int32, ParameterDirection.Output);
                CreditParams.Add("IdClient", credit.IdClient);
                CreditParams.Add("TypeDeCredit", credit.TypeDeCredit);
                CreditParams.Add("Date", credit.Date);
                CreditParams.Add("Montant", credit.Montant);
                CreditParams.Add("DatePaye", credit.DatePaye);
                CreditParams.Add("Taxe", credit.Taxe);
                CreditParams.Add("Comission", credit.Comission);

                var result = _db.ExecuteScalar("sp_Credit_AddCredit", param: CreditParams, commandType: CommandType.StoredProcedure);

                var ValueIdCredit = CreditParams.Get<Int32>("IdCredit");
                credit.Id = ValueIdCredit;

                return credit;
            }
        }


        public Credit UpdateCredit(Credit credit)
        {
            using (var _db = new SqlConnection(ConnectionRepository.SqlConnection()))
            {
                _db.Open();

                var CreditParams = new DynamicParameters();
                CreditParams.Add("IdCredit");
                CreditParams.Add("IdClient", credit.IdClient);
                CreditParams.Add("TypeDeCredit", credit.TypeDeCredit);
                CreditParams.Add("Date", credit.Date);
                CreditParams.Add("Montant", credit.Montant);
                CreditParams.Add("DatePaye", credit.DatePaye);
                CreditParams.Add("Taxe", credit.Taxe);
                CreditParams.Add("Comission", credit.Comission);

                var result = _db.Execute("sp_Credit_UpdateCredit", param: CreditParams, commandType: CommandType.StoredProcedure);
                return result > 0 ? credit : new Credit();
            }
        }


        public bool DeleteCredit(string IdCredit)
        {
            using (var _db = new SqlConnection(ConnectionRepository.SqlConnection()))
            {
                _db.Open();

                var IdCreditParam = new DynamicParameters();
                IdCreditParam.Add("IdCredit", IdCredit);

                var result = _db.Execute("sp_Credit_DeleteCredit", param: IdCreditParam, commandType: CommandType.StoredProcedure);
                return result > 0;
            }
        }
    }
}
