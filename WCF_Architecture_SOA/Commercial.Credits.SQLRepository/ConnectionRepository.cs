﻿namespace Commercial.Credits.SQLRepository
{
    using System.Configuration;

    public class ConnectionRepository
    {
        public static string SqlConnection()
        {
            return ConfigurationManager.ConnectionStrings["WCFSOADB"].ToString();
        }
    }
}
