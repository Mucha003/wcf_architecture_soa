﻿namespace Commercial.Credits.SQLRepository
{
    using Domain;
    using RepositoryContrat;
    using Dapper;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Data;

    public class ClientRepository : IClientRepository
    {
        public IEnumerable<Client> GetAll()
        {
            using (var _db = new SqlConnection(ConnectionRepository.SqlConnection()))
            {
                _db.Open();
                var _clientList = _db.Query<Client>("sp_Client_GetAllClient", commandType: CommandType.StoredProcedure);
                return _clientList;
            }
        }

        public Client GetClient(string pDocumentID)
        {
            using (var _db = new SqlConnection(ConnectionRepository.SqlConnection()))
            {
                _db.Open();

                var pIdDoc = new DynamicParameters();
                // le param qui est utilise dans SqlServer
                pIdDoc.Add("pNumDoc", pDocumentID);

                // <Client> ==> tout ce qi va renvoyer cette proc va etre mappe a
                // notre Model Client du Domain.
                var _client = _db.QuerySingle<Client>("sp_Client_ObtenirClient", param: pIdDoc, commandType: CommandType.StoredProcedure);

                return _client;
            }
        }
    }
}
