﻿namespace Commercial.Credits.RepositoryContrat
{
    using Domain;
    using System.Collections.Generic;

    public interface IClientRepository
    {
        IEnumerable<Client> GetAll();
        Client GetClient(string pDocumentID);
    }
}
