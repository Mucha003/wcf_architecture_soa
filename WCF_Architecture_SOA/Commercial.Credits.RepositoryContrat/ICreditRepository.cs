﻿namespace Commercial.Credits.RepositoryContrat
{
    using Domain;
    using System.Collections.Generic;

    public interface ICreditRepository
    {
        IEnumerable<Credit> GetAll();

        Credit RegistrerCredit(Credit credit);

        Credit UpdateCredit(Credit credit);

        bool DeleteCredit(string IdCredit);
    }
}
