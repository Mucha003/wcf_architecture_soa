﻿namespace Commercial.Credits.Implementation
{
    using BL;
    using Contracts;
    using Domain;
    using System;
    using System.ServiceModel;
    using System.Collections.Generic;

    public class ClientService : IClientService
    {
        public IEnumerable<Client> GetAll()
        {
            using (var _bl = new ClientBL())
            {
                return _bl.GetAll();
            }
        }

        public Client GetClient(string pDocumentID)
        {
            try
            {
                using (var _bl = new ClientBL())
                {
                    return _bl.GetClient(pDocumentID);
                }
            }
            catch (Exception ex)
            {
                var erreurs = new Erreurs()
                {
                    CodeErreur = "0001",
                    Message = "Numero de doc incorrect",
                    Description = "Il n'existe personne avec cette numero de carte"
                };

                throw new FaultException<Erreurs>(erreurs);
            }
        }
    }
}
