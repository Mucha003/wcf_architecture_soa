﻿namespace Commercial.Credits.Implementation
{
    using BL;
    using Contracts;
    using Domain;
    using System.Collections.Generic;

    public class CreditService : ICreditService
    {
        public IEnumerable<Credit> GetAll()
        {
            using (var _bl = new CreditBL())
            {
                return _bl.GetAll();
            }
        }

        public Credit RegistrerCredit(Credit credit)
        {
            using (var _bl = new CreditBL())
            {
                return _bl.RegistrerCredit(credit);
            }
        }

        public Credit UpdateCredit(Credit credit)
        {
            using (var _bl = new CreditBL())
            {
                return _bl.UpdateCredit(credit);
            }
        }

        public bool DeleteCredit(string IdCredit)
        {
            using (var _bl = new CreditBL())
            {
                return _bl.DeleteCredit(IdCredit);
            }
        }
    }
}
