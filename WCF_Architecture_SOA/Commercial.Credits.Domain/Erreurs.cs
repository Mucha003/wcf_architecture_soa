﻿namespace Commercial.Credits.Domain
{
    using System.Runtime.Serialization;

    [DataContract]
    public class Erreurs
    {
        [DataMember]
        public string CodeErreur { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
