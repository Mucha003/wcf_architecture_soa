﻿namespace Commercial.Credits.Domain
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class Credit
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int IdClient { get; set; }

        [DataMember]
        public int TypeDeCredit { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public decimal Montant { get; set; }

        [DataMember]
        public DateTime DatePaye { get; set; }

        [DataMember]
        public decimal Taxe { get; set; }

        [DataMember]
        public decimal Comission { get; set; }
    }
}
