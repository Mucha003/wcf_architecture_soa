﻿namespace Commercial.Credits.Domain
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class Client
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Nom { get; set; }

        [DataMember]
        public string Prenom { get; set; }

        [DataMember]
        public string DocType { get; set; }

        [DataMember]
        public string DocNum { get; set; }

        [DataMember]
        public string Sexe { get; set; }

        [DataMember]
        public DateTime DateOfBird { get; set; }

        [DataMember]
        public string Direction { get; set; }

        [DataMember]
        public string CodePoste { get; set; }

        [DataMember]
        public string EtatCivil { get; set; }
    }
}
